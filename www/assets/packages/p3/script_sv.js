var btn1 = document.querySelector('div.image-centered > div:nth-child(1)');
if (btn1) {
  btn1.addEventListener('click', function () {
    // console.log('btn1 clicked');
  });
}

var btn2 = document.querySelector('div.image-centered > div:nth-child(2)');
if (btn2) {
  btn2.addEventListener('click', function () {
    // console.log('btn2 clicked');
  });
}

var btn3 = document.querySelector('div.image-centered > div:nth-child(3)');
if (btn3) {
  btn3.addEventListener('click', function () {
    // console.log('btn3 clicked');
  });
}

var btn4 = document.querySelector('div.image-centered > div:nth-child(4)');
if (btn4) {
  btn4.addEventListener('click', function () {
    // console.log('btn4 clicked');
  });
}

var nextBtnPayment = document.querySelector('.payment .next-btn');
if (nextBtnPayment) {
  nextBtnPayment.addEventListener('click', function () {
      window.location.href = 'hsa://confirm';
  });
}


var boxies = document.querySelectorAll(".boxy");

for (var i = 0; i < boxies.length; i++) {
  boxies[i].addEventListener('click', function () {

    // reset
    for (var i = 0; i < boxies.length; i++) {
      boxies[i].setAttribute('class', 'boxy');
    }

    this.setAttribute('class', 'boxy selected');
  });
}

var extras = document.querySelectorAll(".extra");

for (var i = 0; i < extras.length; i++) {
  extras[i].addEventListener('click', function () {

    // reset
    for (var i = 0; i < extras.length; i++) {
      extras[i].setAttribute('class', 'extra');
    }

    this.setAttribute('class', 'extra selected');
  });
}

var theNextBtn = document.querySelector(".the-next-btn");
var accept = document.getElementById("accept");
≈