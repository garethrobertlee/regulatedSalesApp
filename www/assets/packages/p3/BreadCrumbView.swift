//
//  BreadCrumbView.swift
//  HSASalesDemo
//
//  Created by James on 14/10/2014.
//  Copyright (c) 2014 MoJoSo. All rights reserved.
//

import UIKit

class BreadCrumbView: UIView {
    
    
    var buttonSteps: [UIButton] = []
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        var viewBindingsDict: NSMutableDictionary = NSMutableDictionary()
        
        //var buttonStep: [UIButton] = []
        var varrow: [UILabel] = []
        let buttonLabels = ["Introduction", "Cover Plans", "Plan Details", "Personalise", "Review", "Payment", "Confirm"]
        
        for i in 0...6 {
            var v = UILabel()
            v.text  = " > "
            v.setTranslatesAutoresizingMaskIntoConstraints(false)
            varrow.append(v)
            
            var button = UIButton()
            button.setTitle(buttonLabels[i], forState: .Normal)
            button.sizeToFit()
            button.tag = i + 1
            button.setTitleColor(UIColor.blackColor(), forState: .Normal)
            button.setTitleColor(UIColor.grayColor(), forState: .Disabled)
            button.titleLabel?.font = UIFont.systemFontOfSize(14.0)
            button.setTranslatesAutoresizingMaskIntoConstraints(false)
            buttonSteps.append(button)
            
            viewBindingsDict.setValue(buttonSteps[i], forKey: "buttonStep\(i)")
            viewBindingsDict.setValue(varrow[i], forKey: "arrow\(i)")
            self.addSubview(buttonSteps[i])
            if i < 6 {
                self.addSubview(varrow[i])
                self.addConstraint(NSLayoutConstraint(item: varrow[i], attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0))
            }
            
            self.addConstraint(NSLayoutConstraint(item: buttonSteps[i], attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0))
            
        }
        
        let hConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-(40)-[buttonStep0]-[arrow0]-[buttonStep1]-[arrow1]-[buttonStep2]-[arrow2]-[buttonStep3]-[arrow3]-[buttonStep4]-[arrow4]-[buttonStep5]-[arrow5]-[buttonStep6]", options: NSLayoutFormatOptions(0), metrics: nil, views:viewBindingsDict)
        self.addConstraints(hConstraints)
        
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRectMake(0, 43, 768, 1)
        bottomBorder.backgroundColor =  UIColor(red: 204/255, green: 204/255, blue: 204/255, alpha: 1.0).CGColor
        self.layer .addSublayer(bottomBorder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

}
