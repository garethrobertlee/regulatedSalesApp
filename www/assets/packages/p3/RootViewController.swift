//
//  RootViewController.swift
//  HSASalesDemo
//
//  Created by James on 26/09/2014.
//  Copyright (c) 2014 MoJoSo. All rights reserved.
//

import UIKit
import CoreData

class RootViewController: UIViewController {
   
    lazy var managedObjectContext : NSManagedObjectContext? = {
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        if let managedObjectContext = appDelegate.managedObjectContext {
            return managedObjectContext
        }
        else {
            return nil
        }
        }()
   
    override func viewDidLoad() {
        
        //let newItem = NSEntityDescription.insertNewObjectForEntityForName("SaleItem", inManagedObjectContext: self.managedObjectContext!) as SaleItem
        
        //newItem.payload = "Wrote Core Data Item!";
        //newItem.status = 1;
        
        //NSLog("DEBUG: test: " + newItem.payload);
        

        
        super.viewDidLoad()

        self.navigationItem.title = "Cancel"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        switch segue.identifier! as String  {
        case "showAddPlan":
            break
        case "showRecordings":
            let splitViewController: UISplitViewController = segue.destinationViewController as UISplitViewController
            splitViewController.preferredDisplayMode = UISplitViewControllerDisplayMode.AllVisible
            //splitViewController.delegate = splitViewController.viewControllers.last
        default:
            break
        }
    }
    


}
