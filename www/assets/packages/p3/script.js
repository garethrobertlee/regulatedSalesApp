/*
var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    x = w.innerWidth || e.clientWidth || g.clientWidth,
    y = w.innerHeight|| e.clientHeight|| g.clientHeight;
console.log(x + ' × ' + y); // should be 768x1024 at all times
*/

var btn1 = document.querySelector('div.image-centered > div:nth-child(1)');
if (btn1) {
  btn1.addEventListener('click', function () {
                        window.open("hsa://2a","_self")
  });
}

var btn2 = document.querySelector('div.image-centered > div:nth-child(2)');
if (btn2) {
    btn2.addEventListener('click', function () {
                          window.open("hsa://2b","_self")  });
}

var btn3 = document.querySelector('div.image-centered > div:nth-child(3)');
if (btn3) {
    btn3.addEventListener('click', function () {
                          window.open("hsa://2c","_self")  });
}

var btn4 = document.querySelector('div.image-centered > div:nth-child(4)');
if (btn4) {
    btn4.addEventListener('click', function () {
                          window.open("hsa://2d","_self")  });
}

var nextBtnPayment = document.querySelector('.payment .next-btn');
if (nextBtnPayment) {
    nextBtnPayment.addEventListener('click', function () {
                                    window.location.href = 'hsa://confirm';
                                    });
}

var boxies = document.querySelectorAll(".boxy");

for (var i = 0; i < boxies.length; i++) {
  boxies[i].addEventListener('click', function () {

    // reset
    for (var j = 0; j < boxies.length; j++) {
      boxies[j].setAttribute('class', 'boxy');
    }

    this.setAttribute('class', 'boxy selected');
    excess = this.getAttribute('excess')
    window.open("hsa://excess/"+excess,"_self")
  });
}

var extras = document.querySelectorAll(".extra");

for (var i = 0; i < extras.length; i++) {
  extras[i].addEventListener('click', function () {
    extra = this.getAttribute('extra')
    window.open("hsa://extra/"+extra,"_self")
    // reset
    for (var j = 0; j < extras.length; j++) {
      extras[j].setAttribute('class', 'extra');
    }

    this.setAttribute('class', 'extra selected');
  });
}

var theNextBtn = document.querySelector(".the-next-btn");
var accept = document.getElementById("accept");

if (accept) {
    accept.addEventListener('change', function () {
                            theNextBtn.setAttribute('class', 'the-next-btn'); //removes the disabled class
                            });
}

if (theNextBtn) {
    theNextBtn.addEventListener('click', function () {
                                window.location.href = 'hsa://payment';
                                });
}

var checkboxes = document.querySelectorAll(".checkbox.second");

for (var i = 0; i < checkboxes.length; i++) {
    checkboxes[i].addEventListener('click', function () {
                                   
                                   // reset
                                   // for (var i = 0; i < checkboxes.length; i++) {
                                   //   checkboxes[i].setAttribute('class', 'checkbox');
                                   // }
                                   
                                   this.setAttribute('class', 'checkbox second checked');
                                   });
}
