//
//  ViewController.swift
//  HSASalesDemo
//
//  Created by James on 22/09/2014.
//  Copyright (c) 2014 MoJoSo. All rights reserved.
//

import UIKit
import AVFoundation
import JavaScriptCore

struct PageDescription {
    let time: Float64
    var image: String?
    var title: String
    var subText: String
    var action: String?
}

enum SalesStep: Float {
    case Introduction = 1.0,
    CoverWeOffer = 2.0,
    PlanDetails1 = 3.0,
    PlanDetails2 = 3.1,
    PlanDetails3 = 3.2,
    CoverOptions = 4.0,
    Review = 5.0,
    Payment = 6.0,
    Confirm = 7.0
}

class SalesViewController: UIViewController, AVCaptureFileOutputRecordingDelegate, UIWebViewDelegate, UIScrollViewDelegate, UIAlertViewDelegate, UIPopoverPresentationControllerDelegate {

    var currentSalesStep: SalesStep = .Introduction
        
    @IBOutlet var imagePreview : UIView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var videoHolderView: UIView!
    @IBOutlet var webView: UIWebView!
    @IBOutlet var footerView: UIView!
    @IBOutlet var footer1: UIButton!
    @IBOutlet var footer2: UIButton!
    @IBOutlet var footer3: UIButton!
    @IBOutlet var footer4: UIButton!
    @IBOutlet var footer5: UIButton!
    @IBOutlet var footer6: UIButton!
    @IBOutlet var footer7: UIButton!
    @IBOutlet var footerOffsetConstraint: NSLayoutConstraint!
    @IBOutlet var priceRightConstraint: NSLayoutConstraint!
    
    @IBOutlet var viewPricePanel: UIView!
    @IBOutlet var labelSelectedProductName: UILabel!
    @IBOutlet var labelSelectedExcess: UILabel!
    @IBOutlet var labelSelectedOption: UILabel!
    @IBOutlet var labelPriceQuote: UILabel!
    @IBOutlet var labelPound: UILabel!
    @IBOutlet var labelAsterisk: UILabel!
    @IBOutlet var buttonApply: UIButton!
    @IBOutlet var labelApply: UILabel!
    
    @IBOutlet var breadCrumbView: BreadCrumbView!
    
    var delegate : AVCaptureFileOutputRecordingDelegate?
    var session : AVCaptureSession!
    var fileOutput : AVCaptureMovieFileOutput!
    var previewLayer : AVCaptureVideoPreviewLayer!
    
    var recording = false
    var player: AVPlayer!
    var playerLayer: AVPlayerLayer!
    var salesSequenceViews: [SalesSequenceView] = []
    
    var playerTimeObserver: AnyObject?
    var videoLayoutConstraints: [NSLayoutConstraint] = []
    var popoverContent: PopupContentViewController?
    
    var excess_choice: String = "0"
    var options_choice: String = "none"
    
    var selectedPlan: String? = "2d"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // initial view configuration
        footerOffsetConstraint.constant = -77;
        navigationItem.title = "Add a Cover Plan"
        popoverContent = self.storyboard?.instantiateViewControllerWithIdentifier("Browser") as? PopupContentViewController
        webView.scrollView.bounces = false
        navigationController?.navigationItem.rightBarButtonItem = nil
        
        
        NSNotificationCenter.defaultCenter().addObserverForName(AVPlayerItemDidPlayToEndTimeNotification, object: nil, queue: NSOperationQueue.mainQueue()) { (NSNotification) -> Void in
            
            // Video finished
            switch self.currentSalesStep {
            case .CoverWeOffer:
                // enable selection of products
                self.loadWebView("cover_we_offer",webView: self.webView!)
            case .CoverOptions:
                // enable application button
                self.buttonApply.enabled = true
                self.labelPriceQuote.textColor = UIColor.whiteColor()
                self.labelPound.textColor = UIColor.whiteColor()
                self.labelAsterisk.textColor = UIColor.whiteColor()
                self.labelApply.hidden = false
            default:
                self.salesSequenceViews.last?.enableAction(self)
            }
        }
        
   
    }
    
    override func viewDidAppear(animated: Bool) {
        
        player = AVPlayer()
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        playerLayer.frame = videoHolderView.bounds
        videoHolderView.layer.addSublayer(playerLayer)
        
        player.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions.New, context: UnsafeMutablePointer<Void>())
        
        startSalesStep(currentSalesStep);
    }
    
    override func viewWillDisappear(animated: Bool) {
        player.pause()
        player.removeObserver(self, forKeyPath: "status")
        
        //player.replaceCurrentItemWithPlayerItem(nil)
        
        // stop recording if we are
        if recording {
            
            self.fileOutput.stopRecording()
            
            /*
            for input in session.inputs {
                session.removeInput(input as AVCaptureInput)
            }
            for output in session.outputs {
                session.removeOutput(output as AVCaptureOutput)
            }*/
        }
    }

    // KVO to start playing video automatically when its ready
    override func observeValueForKeyPath(keyPath: String!, ofObject object: AnyObject!, change: [NSObject : AnyObject]!, context: UnsafeMutablePointer<Void>) {
        if player.status == AVPlayerStatus.ReadyToPlay {
            //player.play()
        }
    }
    
    // setup camera to be ready to record
    func initCamera() -> Bool {
        var cameraFront : AVCaptureDevice?
        var audioMic: AVCaptureDevice?
        
        var devices : NSArray = AVCaptureDevice.devices()
        for device: AnyObject in devices{
            if device.position == AVCaptureDevicePosition.Front{
                cameraFront = device as? AVCaptureDevice
            }
        }
        audioMic = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeAudio)
        
        delegate=self
        
        var error = NSErrorPointer()
        
        if (cameraFront != nil) {
            var deviceInput : AVCaptureInput =  AVCaptureDeviceInput.deviceInputWithDevice(cameraFront, error: error) as AVCaptureInput
            var audioInput : AVCaptureInput = AVCaptureDeviceInput.deviceInputWithDevice(audioMic, error: error) as AVCaptureInput
            
            session=AVCaptureSession()
            session.addInput(deviceInput as AVCaptureInput)
            session.addInput(audioInput as AVCaptureInput)
            
            fileOutput = AVCaptureMovieFileOutput()
            
            session.addOutput(fileOutput)
            
            /*
            var previewLayer: AVCaptureVideoPreviewLayer = AVCaptureVideoPreviewLayer.layerWithSession(session) as AVCaptureVideoPreviewLayer
            previewLayer.frame = self.imagePreview.bounds
            imagePreview.layer.addSublayer(previewLayer)
            */
            
            session.sessionPreset = AVCaptureSessionPreset640x480
            session.startRunning()
            
            player = AVPlayer()
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
            playerLayer.frame = videoHolderView.bounds
            videoHolderView.layer.addSublayer(playerLayer)
            player.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions.New, context: UnsafeMutablePointer<Void>())

            return true
        }
        else {
            return false
        }
    }
    
    @IBAction func record(sender : UIButton?) {
        
        let deviceCanRecordVideo = initCamera()
        
        if recording {
            //println("Stop")
            self.fileOutput.stopRecording()
        }
        else if deviceCanRecordVideo {
            var formatter: NSDateFormatter = NSDateFormatter()
            formatter.dateFormat = "MMM-dd HH-mm"
            let dateTimePrefix: String = formatter.stringFromDate(NSDate())
            
            let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
            
            let documentsDirectory = paths[0] as String
            
            var filePath:String? = nil
            var fileNamePostfix = 0
            
            do {
                filePath = "\(documentsDirectory)/\(dateTimePrefix)-\(fileNamePostfix++).mp4"
            } while (NSFileManager.defaultManager().fileExistsAtPath(filePath!))
            
            let outputFileURL = NSURL(fileURLWithPath: filePath!)
            fileOutput.startRecordingToOutputFileURL(outputFileURL, recordingDelegate: delegate)
            
            let recordButton = UIBarButtonItem(image: UIImage(named: "RecordingIcon"), style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
            navigationItem.rightBarButtonItem = recordButton
            
            //println("Started recording")
        }
        else {
            // simulate recording for simulator
            //println("Started recording")
        }
    }
    
    func captureOutput(captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAtURL outputFileURL: NSURL!, fromConnections connections: [AnyObject]!, error: NSError!){
        recording=false
        //println("Recording saved to: \(outputFileURL)")
    }
    
    func captureOutput(captureOutput: AVCaptureFileOutput!, didStartRecordingToOutputFileAtURL fileURL: NSURL!, fromConnections connections: [AnyObject]!) {
        recording=true
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        pageControl.currentPage = Int(floor((scrollView.contentOffset.x + scrollView.frame.size.width/2)/scrollView.frame.size.width))
    }

    func advanceToPage(page: Int) {
        println(kCFCoreFoundationVersionNumber)
        let margin = (kCFCoreFoundationVersionNumber > kCFCoreFoundationVersionNumber_iOS_7_1) ? 0.0 : 20.0
        scrollView.setContentOffset(CGPointMake(CGFloat(page) * (self.scrollView.frame.width - CGFloat(margin)), 0.0), animated: true)
    }
    
    func startSalesStep(salesStep: SalesStep) {
        
        var allContent: [PageDescription]
        var videoURL: NSURL? = nil
        configureBreadCrumbs(salesStep)
        
        switch salesStep {
        case .Introduction:
            allContent = [PageDescription(time: 0.0, image: "ShieldIcon", title: "Cover for a range of home heating emergencies and breakdowns", subText: "Get boiler cover and central heating insurance to ensure you have access to a network of HomeServe approved engineers in the event of a breakdown.", action: nil),
                PageDescription(time: 7.1, image: "PadlockIcon", title: "You are protected everystep of the way", subText: "For your protection,  insurance policies are covered by the Financial Conduct Authority (FCA) and this session will be video recorded.", action: nil),
                PageDescription(time: 17, image: "SignpostIcon", title: "We'll guide you through the process", subText: "Our videos will help you through the application process.  If at any time you don’t want to proceed, simply press the cancel button above.", action: "   Next   ")
            ]
            videoURL = NSBundle.mainBundle().URLForResource("1", withExtension: "mp4")
            scrollView.hidden = false
            webView.hidden = true
            viewPricePanel.hidden = true
            hideFooterView()
            expandVideo()
            
        case .CoverWeOffer:
            videoURL = NSBundle.mainBundle().URLForResource("2", withExtension: "mp4")
            shrinkVideo()
            scrollView.hidden = true
            webView.hidden = false
            viewPricePanel.hidden = true
            
            allContent = []
            
            loadWebView("cover_we_offer_disabled", webView: webView!)
            
        case .PlanDetails1:
            videoURL = NSBundle.mainBundle().URLForResource("3a", withExtension: "mp4")
            expandVideo()
            webView.hidden = true
            loadWebView("blank",webView: webView!)
            viewPricePanel.hidden = true
            showFooterView()
            
            scrollView.hidden = false
            
            switch selectedPlan! {
            case "2d":  // Cover 8 with boiler service
                allContent = [PageDescription(time: 0.0, image: "ShieldIcon", title: "HomeServe Cover 8", subText: "Help protect your home's gas central heating system (including your gas boiler) against an unexpected breakdown or repair with Gas Central Heating Breakdown Cover\nAn annual boiler service from a HomeServe approved Gas Safe™ registered engineer can help to ensure that your gas boiler is running safely and efficiently.\nThis policy is sold, arranged and administered by HomeServe and underwritten by Inter Partner Assistance SA", action: "   Next   "),
                ]
                
            default:
                allContent = []
            }
            
        case .PlanDetails2:
            videoURL = nil
            
            switch selectedPlan! {
            case "2d":  // Cover 8 with boiler service
                allContent = [PageDescription(time: 0.0, image: "ShieldIcon", title: "What's included", subText: "• Water supply pipe\n• Plumbing and drains\n• Electrical wiring\n• Pest infestations\n• Security and roofing incidents\n• Internal gas supply pipe\n• Gas central heating breakdown (including your boiler)", action: "   Next   "),
                ]
                
            default:
                allContent = []
            }
            
        case .PlanDetails3:
            videoURL = NSBundle.mainBundle().URLForResource("3b", withExtension: "mp4")
            
            switch selectedPlan! {
            case "2d":  // Cover 8 with boiler service
                allContent = [
                    PageDescription(time: 0.0, image: "ShieldIcon", title: "Who Can Buy HomeServe Cover 8?", subText: "• People who are homeowners but not landlords / tenants\n• People who live in houses, bungalows and flats But not mobile homes and bedsits\n• Homes heated with gas but not electric heating\n\nFor full eligibility information please see 'Am I eligible?' below", action: "   Next   "),
                ]
                
            default:
                allContent = []
            }
            
        case .CoverOptions:
            videoURL = NSBundle.mainBundle().URLForResource("4", withExtension: "mp4")
            shrinkVideo()
            scrollView.hidden = true
            webView.hidden = false
            allContent = []
            viewPricePanel.hidden = false
            showFooterView()
            labelSelectedProductName.text = "HomeServe Cover 8"
            labelSelectedExcess.text = "+ £0 excess"
            labelSelectedOption.text = ""
            labelPriceQuote.text = "19.50"
            
            buttonApply.setBackgroundImage(UIImage(named: "green_button"), forState: .Normal)
            buttonApply.setBackgroundImage(UIImage(named: "grey_button"), forState: .Disabled)
            buttonApply.enabled = false
            buttonApply.hidden = false
            labelApply.hidden = false 
            labelPriceQuote.textColor = UIColor.blackColor()
            labelAsterisk.textColor = UIColor.blackColor()
            labelPound.textColor = UIColor.blackColor()
            labelApply.hidden = true
            buttonApply.addTarget(self, action: Selector("completeSalesStep"), forControlEvents: UIControlEvents.TouchUpInside)
            priceRightConstraint.constant = -85
            
            loadWebView("cover_options",webView: webView!)
            
        case .Review:
            videoURL = NSBundle.mainBundle().URLForResource("5A", withExtension: "mp4")
            shrinkVideo()
            scrollView.hidden = true
            webView.hidden = false
            allContent = []
            viewPricePanel.hidden = false
            buttonApply.hidden = true
            labelApply.hidden = true
            labelPriceQuote.textColor = UIColor.blackColor()
            labelAsterisk.textColor = UIColor.blackColor()
            labelPound.textColor = UIColor.blackColor()
            showFooterView()
            
            loadWebView("5-review" ,webView: webView!)
            webView.scrollView.scrollEnabled = false
            priceRightConstraint.constant = 0
            
        case .Payment:
            videoURL = NSBundle.mainBundle().URLForResource("7", withExtension: "mp4")
            shrinkVideo()
            scrollView.hidden = true
            webView.hidden = false
            allContent = []
            viewPricePanel.hidden = false
            buttonApply.hidden = true
            labelApply.hidden = true
            labelPriceQuote.textColor = UIColor.blackColor()
            labelAsterisk.textColor = UIColor.blackColor()
            labelPound.textColor = UIColor.blackColor()
            showFooterView()
            
            loadWebView("6-payment" ,webView: webView!)
            webView.scrollView.scrollEnabled = false
            priceRightConstraint.constant = 0
            
        case .Confirm:
            videoURL = NSBundle.mainBundle().URLForResource("8", withExtension: "mp4")
            shrinkVideo()
            scrollView.hidden = true
            webView.hidden = false
            allContent = []
            viewPricePanel.hidden = false
            buttonApply.hidden = true
            labelApply.hidden = true
            labelPriceQuote.textColor = UIColor.blackColor()
            labelAsterisk.textColor = UIColor.blackColor()
            labelPound.textColor = UIColor.blackColor()
            priceRightConstraint.constant = 0
            showFooterView()
            
            loadWebView("confirm" ,webView: webView!)
            webView.scrollView.scrollEnabled = false 
            
        default:
            allContent = []
        }
        
        if (videoURL != nil) {
            if (player != nil) {
                let playerItem = AVPlayerItem(URL: videoURL)
                player.replaceCurrentItemWithPlayerItem(playerItem)
            }

            
            let triggerTimes = allContent.map() { $0.time }
            
            if (triggerTimes.count > 0) {
                let triggerTimeValues = allContent.map() {
                    NSValue(CMTime: CMTimeMakeWithSeconds($0.time, 1))
                }
                
                let q = dispatch_get_main_queue()
                playerTimeObserver = player.addBoundaryTimeObserverForTimes(triggerTimeValues, queue:q ) { () -> Void in
                    let currentSecond = CMTimeGetSeconds(self.player.currentTime() )
                    var delta = 100.0
                    var currentSlide = 0
                    for (i, time) in enumerate(triggerTimes) {
                        var lastDelta = delta
                        delta = abs(currentSecond-time)
                        currentSlide = i
                        if delta > lastDelta {
                            currentSlide = i - 1
                            break
                        }
                        lastDelta = delta
                    }
                    println("test at play time: \(currentSecond) is slide \(currentSlide)")
                    self.advanceToPage(currentSlide)
                }
            }
        }
        
        let contentVisibleSize = scrollView.frame.size
        
        let numberOfPages = allContent.count
        scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * CGFloat(numberOfPages), scrollView.frame.size.height)
        pageControl.numberOfPages = numberOfPages
        pageControl.hidden = (numberOfPages<=1) ? true : false
        
        for v in scrollView.subviews {
            if v.isKindOfClass(SalesSequenceView) {
                v.removeFromSuperview()
            }
        }
        
        for i in 0..<numberOfPages {
            let salesSequenceView = SalesSequenceView()
            salesSequenceView.frame = CGRectMake(CGFloat(i) * scrollView.frame.size.width, 0.0, scrollView.frame.size.width, contentVisibleSize.height)
            scrollView.addSubview(salesSequenceView)
            salesSequenceView.pageDescription = allContent[i]
            salesSequenceViews.append(salesSequenceView)
        }
        
        player!.play()
        currentSalesStep = salesStep
    }
    
    func completeSalesStep() {
        
        // remove time observer if attached to player
        if (playerTimeObserver != nil) {
            player.removeTimeObserver(playerTimeObserver)
            playerTimeObserver = nil
        }
        
        switch currentSalesStep {
        case .Introduction:
            let alert = UIAlertView(title: "The app will now start recording", message: "You can stop at any time and your details are stored securely", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "OK")
            alert.show()
        case .PlanDetails1:
            startSalesStep(.PlanDetails2)
        case .PlanDetails2:
            startSalesStep(.PlanDetails3)
        case .PlanDetails3:
            startSalesStep(.CoverOptions)
        case .CoverOptions:
            startSalesStep(.Review)
        case .Review:
            startSalesStep(.Payment)
        case .Payment:
            startSalesStep(.Confirm)
        case .Confirm:
            fileOutput.stopRecording()
            navigationItem.rightBarButtonItem = nil
        default:
            break
        }
    }
    
    func alertView(alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        switch currentSalesStep {
        case .Introduction:
            println("button \(buttonIndex) pressed")
            if alertView.buttonTitleAtIndex(buttonIndex) == "OK" {
                record(nil)
                for view in salesSequenceViews {
                    view.removeFromSuperview()
                }
                
                startSalesStep(.CoverWeOffer)
            }
            break
            
        default:
            break
        }
    }
    
    func shrinkVideo() {
        
        let ba = CABasicAnimation(keyPath: "transform")
        ba.autoreverses = false;
        ba.duration = 0.5;
        ba.removedOnCompletion = false
        let transformScale = CATransform3DMakeScale(0.5, 0.5, 0.5)
        let transformTranslate = CATransform3DMakeTranslation(-260.0, -60.0, 0.0)
        let transform = CATransform3DConcat(transformScale, transformTranslate)
        ba.toValue = NSValue(CATransform3D: transform)
        
        CATransaction.begin()
        
        CATransaction.setCompletionBlock { () -> Void in
            self.videoHolderView.layer.transform = transform
        }
        
        videoHolderView.layer.addAnimation(ba, forKey: nil)
        CATransaction.commit()
    }
    
    func expandVideo () {
        let ba = CABasicAnimation(keyPath: "transform")
        ba.autoreverses = false;
        ba.duration = 0.5;
        ba.removedOnCompletion = true
        let transformScale = CATransform3DMakeScale(1, 1, 1)
        let transformTranslate = CATransform3DMakeTranslation(0.0, 0.0, 0.0)
        let transform = CATransform3DConcat(transformScale, transformTranslate)
        ba.toValue = NSValue(CATransform3D: transform)
        
        CATransaction.begin()
        
        CATransaction.setCompletionBlock { () -> Void in
            self.videoHolderView.layer.removeAllAnimations()
            self.videoHolderView.layer.transform = CATransform3DIdentity
        }
        
        videoHolderView.layer.addAnimation(ba, forKey: nil)
        CATransaction.commit()
    }
    
    func showFooterView() {
        view.layoutIfNeeded()
        footerOffsetConstraint.constant = 0;
        //popoverContent!.loadWebView("blank")
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func hideFooterView() {
        view.layoutIfNeeded()
        footerOffsetConstraint.constant = -77;
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    func loadWebView(filename: String, webView: UIWebView) {
        // Remember that bundle resources do *not* have directories so all filenames must be unique.
        let homeIndexUrl = NSBundle.mainBundle().URLForResource(filename, withExtension: "htm")
        var urlReq = NSURLRequest(URL: homeIndexUrl!)

        // The magic is loading a request, *not* using loadHTMLString:baseURL:
        webView.loadRequest(urlReq)
    }
    

    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        //println("shouldStartLoadWithRequest \(request): NSURLRequest, \(navigationType.hashValue): UIWebViewNavigationType")
        if (navigationType == UIWebViewNavigationType.Other) {
            if request.URL.scheme=="hsa" {
                switch currentSalesStep {
                case .CoverWeOffer:
                    selectedPlan = request.URL.host
                    startSalesStep(.PlanDetails1)
                case .CoverOptions:
                    //println(request.URL.pathComponents[1])
                    //println(request.URL.pathComponents.last)
                    
                    if request.URL.host! as String == "excess" {
                            //excess_choice = request.URL.pathComponents.last as String
                            labelSelectedExcess.text = "+ £\(excess_choice) excess"
                    }
                    else if request.URL.host! as String ==  "extra" {
                            //options_choice = request.URL.pathComponents.last as String
                        labelSelectedOption.text = (options_choice == "none") ? "" : "+ Annual boiler service"
                    }
                    
                    
                    // TODO pricing based on product
                    let basePrice = 19.50
                    let excess: [String: Double] = ["0": 0, "50": 10.0, "95": 10]
                    let extras: [String: Double] = ["none": 0, "boiler_service": 7.0]
                    let final_price = basePrice - excess[excess_choice]! + extras[options_choice]!
                    
                    //println("Final price is \(final_price)")
                    labelPriceQuote.text = String(format: "%4.2f", final_price)
                case .Review:
                    startSalesStep(.Payment)
                case .Payment:
                    startSalesStep(.Confirm)
                    
                default:
                    break
                }
                return false
            }
            return true
        }
        else {
            return false
        }
    }
    
    @IBAction func footerButtonTouched(sender: UIButton) {
        
        if (popoverContent == nil ) {
            popoverContent = self.storyboard?.instantiateViewControllerWithIdentifier("Browser") as? PopupContentViewController
        }
        
        if (popoverContent != nil ) {
         
            var preferredSize = CGSizeMake(view.frame.size.width - 20, 600)
            var animated = true
            var title: String = ""
            var showAction: () -> ()
            
            showAction = {self.popoverContent!.loadWebView("eligible")}
            
            switch sender.tag {
            case 200:   //eligible
                showAction = {self.popoverContent!.loadWebView("eligible")}
                preferredSize = CGSizeMake(400, 500)
                title = "Am I eligible?"
            case 201:   //important information
                showAction = {self.popoverContent!.loadWebView("important")}
                preferredSize = CGSizeMake(700, 900)
                title = "Important Information"
            case 202:   //faqs
                showAction = {self.popoverContent!.loadWebView("faqs")}
                preferredSize = CGSizeMake(700, 900)
                title = "Frequently Asked Questions"
            case 203:   //change of mind
                showAction = {self.popoverContent!.loadWebView("change_mind")}
                preferredSize = CGSizeMake(400, 500)
                title = "What if I change my mind?"
            case 204:
                showAction = {self.popoverContent!.loadPDF("T&C_HSC8&excess&Annual")}
                popoverContent!.loadWebView("blank")
                preferredSize = CGSizeMake(0, 0)
                animated = false
            case 205:
                showAction = {self.popoverContent!.loadPDF("SOC_HSC8&excess&Annual")}
                popoverContent!.loadWebView("blank")
                preferredSize = CGSizeMake(0, 0)
                animated = false
            case 206:
                showAction = {self.popoverContent!.loadPDF("About HomeServes Services")}
                popoverContent!.loadWebView("blank")
                preferredSize = CGSizeMake(0, 0)
                animated = false
            default:
                showAction = {}
                break
            }
            
            var nav = UINavigationController(rootViewController: popoverContent!)
            nav.modalPresentationStyle = UIModalPresentationStyle.Popover
            var popover = nav.popoverPresentationController
            popover?.sourceView = sender
            popover?.delegate = self
            popover?.permittedArrowDirections = .Down
            self.presentViewController(nav, animated: animated, completion: showAction)
            
            popoverContent!.preferredContentSize = preferredSize
            popoverContent!.navigationItem.title = title
            
        }
    }
    
    func configureBreadCrumbs(salesStep: SalesStep) {
        let buttons = breadCrumbView.buttonSteps
        let step = Float(floor(salesStep.rawValue))
        //println("step \(step)")
        for b in buttons {
            if Float(b.tag) == step {
                b.setTitleColor(UIColor.blueColor(), forState: .Disabled)
                b.enabled = false
            }
            else if Float(b.tag) < salesStep.rawValue {
                b.addTarget(self, action: "jumpStep:", forControlEvents: UIControlEvents.TouchUpInside)
                b.enabled = true
            }
            else {
                b.setTitleColor(UIColor.grayColor(), forState: .Disabled)
                b.enabled = false
            }
        }
    }

    
    func jumpStep(sender: UIButton) {
        //println(sender.tag)
        let newStep = SalesStep(rawValue: Float(sender.tag))
        startSalesStep(newStep!)
    }
    
}

