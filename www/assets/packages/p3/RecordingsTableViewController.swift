//
//  RecordingsTableViewController.swift
//  HSASalesDemo
//
//  Created by James on 09/10/2014.
//  Copyright (c) 2014 MoJoSo. All rights reserved.
//

import UIKit

class RecordingsTableViewController: UITableViewController {

    var recordings: [String] = []
    var detailViewController: PlaybackViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.navigationItem.title = "Recordings"
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: self, action: "goBack")
        
        let controllers = self.splitViewController!.viewControllers
        detailViewController = controllers.last as? PlaybackViewController

        
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let path: String = paths[0] as String
        var error: NSError? = nil
        
        let fileManager = NSFileManager.defaultManager()
        let enumerator:NSDirectoryEnumerator = fileManager.enumeratorAtPath(path)!

        while let element = enumerator.nextObject() as? String {
            if element.hasSuffix("mp4") { // checks the extension
                recordings.append(element)
            }
        }
    }

    func deleteFileAtRow(row: Int) -> Bool {
        let fileManager = NSFileManager.defaultManager()
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let path: String = paths[0] as String
        var error: NSError? = nil
        let URLToDelete = NSURL(fileURLWithPath: path)?.URLByAppendingPathComponent(recordings[row])
        fileManager.removeItemAtURL(URLToDelete!, error: &error)
        return (error == nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return recordings.count
    }

    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as? UITableViewCell
        
        if !(cell != nil) {
            let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "reuseIdentifier")
        }
        
        // Configure the cell...
        //cell!.textLabel.text = recordings[indexPath.row]
        
        return cell!
    }

    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            if deleteFileAtRow(indexPath.row) {
                recordings.removeAtIndex(indexPath.row)
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            }
            
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let path: String = paths[0] as String
        let url = NSURL(fileURLWithPath: path)?.URLByAppendingPathComponent(recordings[indexPath.row])
        detailViewController?.videoURL = url
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "showDetail" {
            let row = tableView.indexPathForSelectedRow()?.row
            let destination = segue.destinationViewController as PlaybackViewController
            let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
            let path: String = paths[0] as String
            let url = NSURL(fileURLWithPath: path)?.URLByAppendingPathComponent(recordings[row!])
            destination.videoURL = url
        }
    }
    
    func goBack() {
        self.dismissViewControllerAnimated(true , completion: nil)
    }

}
