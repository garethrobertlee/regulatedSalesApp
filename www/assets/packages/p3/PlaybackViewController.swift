//
//  PlaybackViewController.swift
//  HSASalesDemo
//
//  Created by James on 09/10/2014.
//  Copyright (c) 2014 MoJoSo. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation


class PlaybackViewController: UIViewController, UISplitViewControllerDelegate {

    @IBOutlet var labelFilename: UILabel!
    
    var videoURL: NSURL?  {
        didSet {
            labelFilename?.text = ""   //videoURL?.path
            //playerViewController?.player = AVPlayer( 
            
            if let avpVC = self.childViewControllers.first as? AVPlayerViewController {
                dispatch_async(dispatch_get_main_queue()) {
                    avpVC.player = AVPlayer(URL: self.videoURL)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelFilename.text = ""
        // Do any additional setup after loading the view.
        
        // Create player view controller and player with URL.
  
    }

    override func viewWillDisappear(animated: Bool) {
        if let avpVC = self.childViewControllers.first as? AVPlayerViewController {
            avpVC.player?.pause()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    required init(coder aDecoder: NSCoder) {
        //fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
