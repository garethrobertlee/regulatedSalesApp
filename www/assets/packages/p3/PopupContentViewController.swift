//
//  PopupContentViewController.swift
//  HSASalesDemo
//
//  Created by James on 11/10/2014.
//  Copyright (c) 2014 MoJoSo. All rights reserved.
//

import UIKit

class PopupContentViewController: UIViewController, UIDocumentInteractionControllerDelegate, UIWebViewDelegate {

    var documentInteractionController: UIDocumentInteractionController?
    @IBOutlet var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadWebView(filename: String) {
        // Remember that bundle resources do *not* have directories so all filenames must be unique.
        let homeIndexUrl = NSBundle.mainBundle().URLForResource(filename, withExtension: "htm")
        var urlReq = NSURLRequest(URL: homeIndexUrl!)
        
        // The magic is loading a request, *not* using loadHTMLString:baseURL:
        webView?.loadRequest(urlReq)
    }
    
    func loadPDF(filename: String) {
        let document = NSBundle.mainBundle().URLForResource(filename, withExtension: "pdf")
        documentInteractionController = setupDocumentInteractionController(document!, interactionDelegate: self)
        documentInteractionController?.presentPreviewAnimated(true)
    }
    
    func setupDocumentInteractionController(fileURL: NSURL, interactionDelegate: UIDocumentInteractionControllerDelegate) -> UIDocumentInteractionController {
        let interactionController = UIDocumentInteractionController(URL: fileURL)
        interactionController.delegate = interactionDelegate
        return interactionController
    }
    
    func documentInteractionControllerViewControllerForPreview(controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func documentInteractionControllerDidEndPreview(controller: UIDocumentInteractionController) {
        dismissViewControllerAnimated(false, completion: nil)
    }
    
}
