//
//  DisplayView.swift
//  HSASalesDemo
//
//  Created by James on 26/09/2014.
//  Copyright (c) 2014 MoJoSo. All rights reserved.
//

import Foundation
import UIKit

class DisplayView: UIView {
    
    var image: UIImage?
    var imageView: UIImageView?
    var headline: String = ""
    var body: String = ""
    
    init(headline: String, body: String) {
        super.init()
        self.headline = headline
        self.body = body
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}