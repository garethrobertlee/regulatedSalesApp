angular.module('rsa.services')
.filter('syncfilter', function (SaleService) {
    var items = SaleService.getAllSales();
    return function (items) {
        var filtered = [];
        for (var i = 0; i < items.length; i++) {
            var item = items[i];
            if (item.videoSyncStatus == false || item.paid == false) {
                filtered.push(item);
            }
        }
        return filtered;
    };
});
