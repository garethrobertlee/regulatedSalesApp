app.controller('SignatureCtrl', function ($scope, $state, ProductsService, SaleService, CaptureService, $ionicPopup,$auth,$http,UserService,$ionicLoading, $ionicModal, $rootScope) {


    $ionicModal.fromTemplateUrl('templates/modals/confirmSign.html', {
        id: 'confirmSignature',
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.oModal3 = modal;
    });

    $scope.closeModal = function(){
        $scope.oModal3.hide();
    }

    $scope.openModal = function(){
        $scope.oModal3.show();
    }

    $scope.currentSale = SaleService.getCurrentSale();
  $scope.sigImageData = "";

  var canvas = document.getElementById("signature");
  var signaturePad = new SignaturePad(canvas);
  $scope.goToRoute = function (routeName) {
      CaptureService.stopCapture();
      //navigator.geolocation.getCurrentPosition(onSuccess, onError);
      SaleService.setCurrentSale($scope.currentSale);
      $state.go(routeName);
  };




    $scope.clearCanvas = function(){
    signaturePad.clear();
  };
  $scope.getSignatureData = function(){
    //var index = dataString.indexOf(",") + 1;
    //dataString = dataString.substring(index);
    $scope.sigImageData = signaturePad.toDataURL();
    $scope.openModal();
  };

    $scope.closeModalClearPad = function(){
        signaturePad.clear();
        $scope.closeModal();
    }
  $scope.confirmSignature = function() {
          $scope.currentSale = SaleService.getCurrentSale();
      $scope.currentSale.name = $scope.currentSale.payload.city;
        $scope.currentSale.signature = $scope.sigImageData;
      $rootScope.sigImageData = $scope.sigImageData;
        SaleService.setCurrentSale($scope.currentSale);
          console.log($scope.currentSale);
          signaturePad.clear();
      $scope.closeModal();
        $scope.goToRoute('app.payment');
  };




  $scope.proceedToPayment = function() {

// Returns signature image as data URL
    //signaturePad.toDataURL();

// Draws signature image from data URL
    //signaturePad.fromDataURL("data:image/png;base64,iVBORw0K...");

// Clears the canvas
    //signaturePad.clear();

// Returns true if canvas is empty, otherwise returns false
    //signaturePad.isEmpty();


  };


  var init = function () {

  };
  init();

});
