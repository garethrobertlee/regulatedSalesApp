app.controller('LoginCtrl', function ($scope, $state, $auth, $location, ModalService, $timeout, $ionicPopup, $ionicLoading, $http, VersionService, $ionicModal,UserService) {

    //$ionicModal.fromTemplateUrl('templates/modals/splashscreen.html', {
    //    id: 'splashscreen',
    //    scope: $scope,
    //    animation: 'slide-in-left'
    //}).then(function(modal) {
    //    $scope.modal = modal;
    //});
    //
    //$scope.openModal = function() {
    //        $scope.modal.show();
    //};
    //
    //$scope.closeModal = function(){
    //    $scope.modal.hide();
    //}

    $scope.authenticateUser = function(loginForm){
        $auth.submitLogin(loginForm)
            .then(function(resp) {
                // handle success response
                UserService.setId(resp.id);
                UserService.setUser(resp);
                UserService.setCompany(resp.company);
                UserService.setTestUser(resp.test_user); //todo make one here, obv
                console.log(window.localStorage['passcode']);
                if (!window.localStorage['passcode']){
                    $location.url('/app/createpincode')
                }else{
                    $location.url('/app/enterpincode')
                }
            })
            .catch(function(resp) {
                // handle error response
                var alertPopup = $ionicPopup.alert({
                    title: 'Login Failed\n\n',
                    template: 'Invalid Login Credentials'
                });
                alertPopup.then(function(res) {
                    // Do nothing
                });
            });
    }

    $scope.handleLoginBtnClick = function(loginForm) {

        $http.post($auth.apiUrl() + '/api/v1/sales/get_latest_version', {}).then(function(response) {

            VersionService.setLatestVersion({
                "major": response.data.major,
                "minor": response.data.minor,
                "rev":   response.data.rev,
                "notes": response.data.notes
            });

            var validation_result = VersionService.validateVersion();

            switch (validation_result)
            {
                case 'major':

                    // An alert dialog
                    $scope.showMajorVersionAlert = function() {
                        var alertPopup = $ionicPopup.alert({
                            title: 'App Update Required',
                            template: 'A mandatory update has been released. Please <a href="https://apps.homeservealliance.com/tracemobile">click here</a> update to continue using this app:'
                        });
                        alertPopup.then(function(res) {
                            // Do nothing
                        });
                    };

                    $scope.showMajorVersionAlert();
                    break;

                case 'minor':
                case 'rev':
                    // An alert dialog
                    $scope.showMinorVersionAlert = function() {
                        var alertPopup = $ionicPopup.alert({
                            title: 'App Update Available',
                            template: 'There is a new version of the app available. Please <a href="https://apps.homeservealliance.com/tracemobile">click here</a> update to unlock the latest fixes and features'
                        });
                        alertPopup.then(function(res) {
                            $scope.authenticateUser(loginForm);
                        });
                    };

                    $scope.showMinorVersionAlert();
                    break;

                case 'none':
                    alert('You have the latest version.');
                    $scope.authenticateUser(loginForm);
            }

        }, function(err) {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
                title: 'Error checking latest app version\n\n',
                template: 'Error checking latest app version\n\n'
            });
            alertPopup.then(function(res) {
                $scope.authenticateUser(loginForm);
            });

            //VersionService.setLatestVersion(VersionService.getLocalVersion());
            //$scope.authenticate_user();

        });
    };

    $scope.passcode = "";

    // creating a pincode
    $scope.create = function(value) {
        if($scope.passcode.length < 4) {
            console.log($scope.passcode)
            // resetting the error message when starting input again
            $scope.error = false;
            $scope.passcode = $scope.passcode + value;

            if($scope.passcode.length == 4) {
                $timeout(function () {
                    console.log("You have created a 4 code that is " + $scope.passcode);
                }, 500);
                localStorage.setItem('passcode', $scope.passcode);
                //$scope.splashScreen();
                $location.url('/app/dashboard')
            }
        }
    };

    //$scope.splashScreen = function(){
    //    $scope.openModal();
    //    $timeout(function() {
    //        //will be directed to / after 3 seconds.
    //        $scope.closeModal();
    //        $location.url('/app/dashboard');
    //    }, 1000);
    //}

    $scope.passcode = "";


    // pincode login
    $scope.add = function(value) {
        if($scope.passcode.length < 4) {
            console.log($scope.passcode)
            // resetting the error message when starting input again
            $scope.error = false;
            $scope.passcode = $scope.passcode + value;

            if($scope.passcode.length == 4) {
                $timeout(function () {
                    console.log("The four digit code was entered and is " + $scope.passcode);
                }, 500);

                // checking if passcode is correct
                if($scope.passcode.length == 4 && $scope.passcode == localStorage.getItem('passcode')){
                 console.log($location.path())
                    $state.go('app.dashboard')
                } else {
                    $scope.error = true;
                    //resetting the pinpad to 0
                    $scope.passcode = "";
                }
            }
        }
    };


    $scope.delete = function() {
        if($scope.passcode.length > 0) {
            $scope.passcode = $scope.passcode.substring(0, $scope.passcode.length - 1);
        }
    };


    var hasBeenLoggedIn = false;

    $scope.goToRoute = function (routeName) {
        $state.go(routeName);
    };

    var init = function () {
    };
    init();

});
