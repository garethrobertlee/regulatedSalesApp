app.controller('NewSaleCtrl', function ($scope, $state, $http, ProductsService, SaleService, CaptureService, UserService, $ionicLoading,$ionicPopup, ModalService,$auth) {

$scope.testUser = UserService.getTestUser();
    console.log($scope.testUser)

    document.addEventListener("deviceready", onDeviceReady, false);
    function onDeviceReady() {
        console.log("navigator.geolocation works well");
    }
    //pincode authentication
    $scope.modal1 = function() {
        ModalService
            .init('templates/modals/pincode.html', $scope)
            .then(function(modal) {
                modal.show();
            });
    };

    $scope.goToRoute = function (routeName) {
        $scope.resetCustomer();
        $state.go(routeName);
    };

    $scope.products = ProductsService.getProducts();
  $scope.currentProduct = $scope.products[0];
  $scope.customer = {};

  $scope.current_sale = null;

  $scope.newSale = function(prod, customer) {

  };

    $scope.resetCustomer = function(){
        $scope.customer = {firstname:"",lastname:"",email:""}
    };


  // A confirm dialog
  $scope.beginSale = function(prod, customer) {
    console.log(prod)
    var confirmPopup = $ionicPopup.confirm({
      title: 'Sales Process Initiating...',
      template: 'Video and audio footage recording is about to commence. Please confirm and hand the device to the customer'
    });
    confirmPopup.then(function(res) {
      if(res) {
        $scope.stripeKeys(prod,customer)

      } else {
        console.log('Cancelled');
      }
    });
  };

    $scope.stripeKeys = function(prod,customer) {

        $http.post($auth.apiUrl() + '/api/v1/sales/get_stripe_info').then(function (response) {

            if (response.data.status == true) {
                //UserService.setStripeKey(response.data.stripe_publishable_key);
                key = response.data.stripe_publishable_key;
                //has retrieved the stripe_publishable_key
                $scope.proceedWithSale(prod,customer,key)
            }

        }, function (err) {

            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
                title: 'error retrieving stripe keys\n\n',
                template: "continue with sale, and take payment at later date?"
                // err.status will contain the status code
            });
            alertPopup.then(function(res) {
                if(res) {
                    $scope.proceedWithSale(prod,customer)

                } else {
                    console.log('Cancelled');
                }
            });
        });

    };

    $scope.proceedWithSale = function (prod,customer,key){
        user = UserService.getUser();
        $scope.currentSale = SaleService.newSale(prod, customer,user,key);
        SaleService.saveSales();
        // TODO - Download this package: ** $scope.currentProduct.packageUrl **
        //  navigator.geolocation.getCurrentPosition($scope.geolocationSuccess());
        // Start capturing video/audio - DISABLE WHEN NOT TESTING IN DEVICE
        navigator.geolocation.getCurrentPosition(onSuccess, onError);
        CaptureService.startCapture(SaleService.getCurrentSale().filename);
        console.log($scope.currentSale)
        $scope.goToRoute('app.displaysale');
    };

    var onSuccess = function(position) {
        //alert('Latitude: '          + position.coords.latitude          + '\n' +
        //'Longitude: '         + position.coords.longitude         + '\n')
        $scope.currentSale.lat = position.coords.latitude;
        $scope.currentSale.lng = position.coords.longitude;
        SaleService.setCurrentSale($scope.currentSale);
    };

// onError Callback receives a PositionError object
//
    var onError = function(error) {
        alert('code: '    + error.code    + '\n' +
        'message: ' + error.message + '\n');
    }

    //angular.element(document).ready(function () {
    //     $scope.modal1();
    //});




    var init = function () {
  };
  init();

});
