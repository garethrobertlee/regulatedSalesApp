app.controller('FinalStatusCtrl', function ($scope, $state, SaleService) {

    $scope.finalStatus = SaleService.getFinalStatus();
    $scope.backtoDashboard = function(){
        SaleService.resetVideo();
        $scope.currentSale = SaleService.getCurrentSale();
        $scope.goToRoute('app.dashboard');
    };
    $scope.goToRoute = function (routeName) {
        $state.go(routeName);
    };

    var init = function () {
    };
    init();

});
