app.controller('MySalesCtrl', function ($scope, $state, ProductsService, SaleService, CaptureService, $ionicModal,$ionicPopup,uploadVideo, $timeout, $interval,$rootScope) {

    $scope.preview = function(sale){
        $scope.sale = sale;
        $scope.basePrice = parseFloat(sale.payload.price);
        $scope.total = parseFloat(sale.payload.price) + parseFloat(sale.payload.extras);
        console.log($scope.total)
        $scope.oModalPreviewSale.show(sale);
    }

    $ionicModal.fromTemplateUrl('templates/modals/previewSale.html', {
        id: 'previewSale',
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.oModalPreviewSale = modal;
    });



    $scope.closeModal = function(){
        $scope.oModalPreviewSale.hide();
    }

  $scope.sales = SaleService.getAllSales();
    console.log($scope.sales)
    $scope.syncAllSales = function(){

        for (var i = 0 ; i < $scope.sales.length; i++) {
            //$scope.sales[i].isSynching = true
            console.log($scope.sales[i])

            if (!$scope.sales[i].id) {
                if ($scope.sales[i].signature) {
                    SaleService.sendQuote($scope.sales[i]).then(function(data){
                        sale = data.sale
                        $scope.uploadAndSync($scope.sales[i])
                    })
                }
                consol.elog(sale)


            }
            else if ($scope.sales[i].videoSyncStatus != true){
                var sale = $scope.sales[i];
                //sale.isSynching = true
                $scope.uploadAndSync(sale)
            }
        }

    };

    $scope.deleteSale = function(sale){
        if (!sale.signature) {
            SaleService.deleteSale(sale)
            $scope.sales = SaleService.getAllSales();
            $scope.closeModal()
        }
    }


    $scope.uploadAndSync = function(sale){
        console.log("this would upload vid")
        console.log(sale)
            sale.syncing = true;
        uploadVideo.sendCurrentFile(sale).then(function(data){
            console.log("data")
            console.log(data)
            sale.syncing = false;
            SaleService.synchedVideo(sale)
            //sale.isSynching = false
            $scope.sales = SaleService.getAllSales();
            console.log($scope.sales)

            //sale.videoSyncStatus = true

        });
    }

  $scope.shouldShowDelete = false;
  $scope.shouldShowReorder = false;
  $scope.listCanSwipe = true;

  $scope.goToRoute = function (routeName) {
    $state.go(routeName);
  };



$scope.progressBar = function() {
    $scope.counter = 0;
    $interval(function () {
        var progressbar = document.getElementById('progress');
        $scope.progressbarValue = progressbar.value;
        var value = parseInt($scope.progressbarValue + 1);
        progressbar.value = value;
        if (progressbar.value === 100) {
            $scope.progressbarValue = 100;
            //document.styleSheets[0].insertRule('progress::-webkit-progress-value { background: rgb(40,200,0) !important; }', 0);
        }
    }, 100, 100);
}

    $scope.makePayment = function(sale) {
        $scope.closeModal() //TODO stripe buttons not accessible if the calling modal not hidden, dont know why yet
        SaleService.makePayment(sale).then(function(result){
            $scope.sales = SaleService.getAllSales();
            console.log($scope.sales)
        });
    };

    var init = function () {

  };
  init();

});


