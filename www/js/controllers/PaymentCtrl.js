app.controller('PaymentCtrl', function ($scope,$state, ProductsService, SaleService, CaptureService,uploadVideo,$http,$ionicModal, $ionicLoading, $ionicPopup, UserService, $location, $rootScope,$timeout) {

    $scope.sigImageData = $rootScope.sigImageData;
    $scope.saleUploaded = false;
    $scope.currentSale = SaleService.getCurrentSale();
    $scope.goToRoute = function (routeName) {
        $state.go(routeName);
    };
    $scope.takePayment = function() {
        alert('TODO - payment stuff...');
        $scope.goToRoute('app.dashboard');
    };
    $scope.backtoDashboard = function(){
        SaleService.resetVideo();
        $scope.currentSale = SaleService.getCurrentSale();
        $scope.goToRoute('app.dashboard');
    };
    $scope.stopRecording = function(){
        CaptureService.stopCapture();
    };
    $scope.uploadVideo = function(){
        uploadVideo.sendAllFiles();
    };
    $scope.uploadCurrentSale = function(){
        uploadVideo.sendCurrentFile($scope.currentSale);
        $scope.goToRoute('app.dashboard');
    };
    $scope.uploadvid = function(){ //remove this temp function
        console.log("test")
        uploadVideo.sendCurrentFile($scope.currentSale);
        $scope.goToRoute('app.dashboard');
    };
    $scope.uploadSale = function (sale) {
        $scope.currentSale = SaleService.getCurrentSale();
        var price = Math.round($scope.currentSale.product.price);
        console.log($scope.currentSale)
        SaleService.sendQuote({"id": ''},$scope.currentSale);
    };
    $scope.proceedWithoutPayment = function(){
        //todo handle what to do with sale here
        SaleService.sendQuote($scope.currentSale).then(function(response){
            console.log(response)
            if (response){
                return $location.url('/app/submittedpayment');
            }
            //$scope.goToRoute('app.dashboard');
        });
        //$scope.goToRoute('app.dashboard');
    };
    $scope.stripe_publishable_key = null;
    $scope.parseFloat = parseFloat;
    $scope.makePayment = function() {
        $scope.currentSale = SaleService.getCurrentSale();
        SaleService.makePayment($scope.currentSale).then(function(result){
            if (result < 15) {
                return $location.url('/app/submittedpayment');
            }else{
                //$scope.nonPayingSale = true;
            }

        });

        stripeWindowlength = document.getElementsByName('stripe_checkout_app').length;
        stripeWindow = document.getElementsByName('stripe_checkout_app')[stripeWindowlength-1];
        stripeWindow.addEventListener ("DOMNodeRemoved", closedStripe);
        function closedStripe(){
            $timeout(function () {
                $scope.nonPayingSale = true;
                $scope.$apply()
            }, 1500);

        }

    };

    var init = function () {
        $scope.saleUploaded = false;
    };
    init();

});
