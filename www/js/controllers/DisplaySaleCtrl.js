app.controller('DisplaySaleCtrl', function ($scope, $state, ProductsService, SaleService, $sce, CaptureService) {


    $scope.saleData = {
    data: 'null',
    rawData: 'null'
  };

  $scope.currentSale = SaleService.getCurrentSale();


    $scope.trustSrc = function() {
        var firstVideo = document.getElementById("sale-frame").contentDocument.getElementById('video1');
        if (firstVideo){
            firstVideo.autoplay = true;
            firstVideo.load();
            console.log(firstVideo)
        }
        return $sce.trustAsResourceUrl($scope.currentSale.product.packageUrl);
    };

  $scope.startRecording = function() {

    CaptureService.startCapture($scope.currentSale.filename);

  };

  $scope.stopRecording = function() {

    CaptureService.stopCapture();

  };

    $scope.reloadIframe = function(){
        document.getElementById('sale-frame').src += '';
        var firstVideo = document.getElementById("sale-frame").contentDocument.getElementById('video1');
        if (firstVideo){
            firstVideo.autoplay = false;
            firstVideo.load();
        }
    }




  $scope.setFormPayload = function(formData) {
    $scope.currentSale = SaleService.getCurrentSale();
    $scope.currentSale.payload = formData;
    SaleService.setCurrentSale($scope.currentSale);
      $scope.reloadIframe();
    $scope.goToRoute('app.signature')
  };

  $scope.goToRoute = function (routeName) {
      $state.go(routeName);
  };

  var init = function () {
  };
  init();

});
