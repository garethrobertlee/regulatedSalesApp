app.controller('DashboardCtrl', function ($scope, $ionicLoading, $state, $auth, $ionicPopup, $ionicModal, $timeout, ModalService, CredentialsService, SaleService,ProductsService, $rootScope) {

    //pincode authentication
    $scope.modal1 = function() {
        ModalService
            .init('templates/modals/pincode.html', $scope)
            .then(function(modal) {
                modal.show();
            });
    };

    $scope.salesNeedingAttention = SaleService.getSalesNeedingAttention();



    $scope.goToRoute = function (routeName) {
        $state.transitionTo(routeName);
    };

    $scope.api_credentials = CredentialsService.getCredentials();

    $scope.action = function () {

    };

    $scope.newSale = function() {
        $scope.goToRoute('app.newsale');
    };
    $scope.mySales = function() {
      $scope.goToRoute('app.mysales');
    };
    $scope.syncSales = function() {
      $scope.goToRoute('app.syncsales');
    };
    $scope.myProducts = function() {
      $scope.goToRoute('app.myproducts');
    };
    $scope.syncProducts = function() {
      $scope.goToRoute('app.syncproducts');
    };
    $scope.loginAgain = function() {
        $scope.goToRoute('app.login');
    };



    //angular.element(document).ready(function () {
    //     $scope.modal1();
    //});



    var init = function () {

      // Set some test products - TODO: Get products from our API
      ProductsService.setTestProducts();

    };
    init();
});

