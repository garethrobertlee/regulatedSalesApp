// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var app = angular.module('rsa', [
    'ionic',
    'rsa.controllers',
    'rsa.services',
    'ngSanitize',
    'ngCordova',
    'ng-token-auth'
])

    .run(function($ionicPlatform, ModalService, $rootScope, $location,$state,SaleService,uploadVideo,CaptureService) {
        $rootScope.modal1 = function() {
            ModalService
                .init('templates/modals/pincode.html', $rootScope)
                .then(function(modal) {
                    modal.show();
                });
        };


        $ionicPlatform.ready(function(){
            //this calls the enter pincode function on resume

            document.addEventListener("pause", function(){
                sale = SaleService.getCurrentSale();
                if (!sale.signature){
                    CaptureService.stopCapture();
                    uploadVideo.deleteVideo(sale)
                    SaleService.deleteSale(sale)
                }

                console.log("i am pausing");
                window.localStorage['active'] = false;
                console.log($location.path())

                window.localStorage['lastlocation'] = $location.path()
                console.log(window.localStorage)
            });

            document.addEventListener("resume", function() {

                console.log("The application is resuming from the background");
                //pulls up pincode on resume
                console.log(window.localStorage['passcode']);
                console.log(window.localStorage['active'])
                if (window.localStorage['active'] == false) {
                    console.log("five")
                    console.log($state)
                    window.localStorage['active'] = true;
                    $state.go('app.enterpincode')
                }
                console.log($location.path())
                if(window.localStorage['passcode']){
                    //opening enter pincode modal
                    //$rootScope.modal1();
                    window.localStorage['active'] = true;
                    $state.go('app.enterpincode')
                }else{
                    // telling you to create pincode
                    window.localStorage['active'] = true;
                    $location.url('/app/createpincode');
                }
            }, false);

            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });

    })


    .config(function($stateProvider, $urlRouterProvider, $authProvider) {

        // the following shows the default values. values passed to this method
        // will extend the defaults using angular.extend

        $authProvider.configure({
            apiUrl: 'https://staging-regulated.herokuapp.com',
            tokenValidationPath: '/auth/validate_token',
            signOutUrl: '/auth/sign_out',
            emailRegistrationPath: '/auth',
            accountUpdatePath: '/auth',
            accountDeletePath: '/auth',
            confirmationSuccessUrl: window.location.href,
            passwordResetPath: '/auth/password',
            passwordUpdatePath: '/auth/password',
            passwordResetSuccessUrl: window.location.href,
            emailSignInPath: '/auth/sign_in',
            storage: 'localStorage',
            proxyIf: function () {
                return false;
            },
            proxyUrl: '/proxy',
            authProviderPaths: {
                github: '/auth/github',
                facebook: '/auth/facebook',
                google: '/auth/google'
            },
            tokenFormat: {
                "access-token": "{{ token }}",
                "token-type": "Bearer",
                "client": "{{ clientId }}",
                "expiry": "{{ expiry }}",
                "uid": "{{ uid }}"
            },
            parseExpiry: function (headers) {
                // convert from UTC ruby (seconds) to UTC js (milliseconds)
                return (parseInt(headers['expiry']) * 1000) || null;
            },
            handleLoginResponse: function (response) {
                window.localStorage['hasBeenLoggedIn'] = true;
                return response.data;

            },
            handleAccountResponse: function (response) {
                return response.data;
            },
            handleTokenValidationResponse: function (response) {
                return response.data;
            }
        });

        $stateProvider

            .state('app', {
                url: "/app",
                abstract: true,
                templateUrl: "templates/menu.html",
                controller: 'AppCtrl'
            })

            .state('app.dashboard', {
                url: "/dashboard",
                cache: false,
                //data: {isActive: true},
                views: {
                    'menuContent': {
                        templateUrl: "templates/dashboard.html",
                        controller: 'DashboardCtrl'
                    }
                }
            })

            .state('app.newsale', {
                url: "/newsale",
                views: {
                    'menuContent': {
                        templateUrl: "templates/newsale.html",
                        controller: 'NewSaleCtrl'
                    }
                }
            })

            .state('app.displaysale', {
                url: "/displaysale",
                views: {
                    'menuContent': {
                        templateUrl: "templates/displaysale.html",
                        controller: 'DisplaySaleCtrl'
                    }
                }
            })

            .state('app.signature', {
                url: "/signature",
                views: {
                    'menuContent': {
                        templateUrl: "templates/signature.html",
                        controller: 'SignatureCtrl'
                    }
                }
            })

            .state('app.payment', {
                url: "/payment",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/payment.html",
                        controller: 'PaymentCtrl'
                    }
                }
            })

            .state('app.mysales', {
                url: "/mysales",
                views: {
                    'menuContent': {
                        templateUrl: "templates/mysales.html",
                        controller: 'MySalesCtrl'
                    }
                }
            })

            .state('app.syncsales', {
                url: "/syncsales",
                views: {
                    'menuContent': {
                        templateUrl: "templates/syncsales.html",
                        controller: 'SyncSalesCtrl'
                    }
                }
            })

            .state('app.myproducts', {
                url: "/myproducts",
                views: {
                    'menuContent': {
                        templateUrl: "templates/myproducts.html",
                        controller: 'MyProductsCtrl'
                    }
                }
            })

            .state('app.login', {
                url: "/login",
                views: {
                    'menuContent': {
                        templateUrl: "templates/login.html",
                        controller: 'LoginCtrl'
                    }
                }
            })

            .state('app.createpincode', {
                url: "/createpincode",
                views: {
                    'menuContent': {
                        templateUrl: "templates/createPincode.html",
                        controller: 'LoginCtrl'
                    }
                }
            })

            .state('app.enterpincode', {
                url: "/enterpincode",
                cache:false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/enterPincode.html",
                        controller: 'LoginCtrl'
                    }
                }
            })

            .state('app.submittedPayment', {
                url: "/submittedpayment",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/submittedPayment.html",
                        controller: 'FinalStatusCtrl'
                    }
                }
            })

            .state('app.syncproducts', {
                url: "/syncproducts",
                views: {
                    'menuContent': {
                        templateUrl: "templates/syncproducts.html",
                        controller: 'SyncProductsCtrl'
                    }
                }
            });

        // if none of the above states are matched, use this as the fallback
        //

        if(window.localStorage['hasBeenLoggedIn'] && window.localStorage['passcode'] && window.localStorage['active'] == true) {
            console.log("first")
            $urlRouterProvider.otherwise('/app/dashboard');
        }
        else if (window.localStorage['hasBeenLoggedIn'] && window.localStorage['passcode']){
            console.log("second")
            console.log(window.localStorage['passcode'])
            $urlRouterProvider.otherwise('/app/enterpincode');

        }else if(window.localStorage['hasBeenLoggedIn'] && !window.localStorage['passcode']){
            console.log("second")
            $urlRouterProvider.otherwise('/app/createpincode');
        }else {
            console.log("third")
            console.log(window.localStorage['hasBeenLoggedIn'])
            console.log(window.localStorage['passcode'])
            console.log(window.localStorage['active'])
            $urlRouterProvider.otherwise('/app/login');
        };
    })

.run(['$rootScope','$state','$location', function ($rootScope, $state, $location) {

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState) {

        console.log(toState.name)
        console.log(window.localStorage)


    });
}])



