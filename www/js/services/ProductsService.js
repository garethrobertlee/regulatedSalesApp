angular.module('rsa.services')
  .factory('ProductsService', [
    '$http', function ($http) {

      var products = [];

      return {
        getProducts: function() {
          return products;
        },
        setProducts: function(prods) {
          products = prods;
        },
        setTestProducts: function() {
          products = [
            {
              id: 5,
              name: 'Super Demo',
              packageUrl: 'assets/packages/demo/index.html',
              img: 'img/jamesdemo.png',
                pricePence: 950
            },
            {
              id: 4,
              name: 'Matts Demo',
              packageUrl: 'assets/packages/p4/index.html',
              img: 'http://www.cwu.org/assets/_files/images/jun_13/cwu__1371034073_House_Insurance.jpg',
                pricePence: 1500
            },
            {
              id: 1,
              name: 'James Demo',
              packageUrl: 'assets/packages/p3/',
              img: 'img/jamesdemo.png',
                pricePence: 2000
            },
            {
              id: 2,
              name: 'Boiler Insurance',
              packageUrl: 'assets/packages/p1/index.html',
              img: 'http://www.centralheatingcover.org.uk/wp-content/uploads/2011/10/boiler-insurance.jpg',
                pricePence: 1100
            },
            {
              id: 3,
              name: 'Home Insurance',
              packageUrl: 'assets/packages/p2/index.html',
              img: 'http://www.cwu.org/assets/_files/images/jun_13/cwu__1371034073_House_Insurance.jpg',
                pricePence: 1000
            }
          ];
        }
      };

    }
  ]);
