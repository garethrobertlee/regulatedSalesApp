angular.module('rsa.services')
    .factory('ModalService', function($ionicModal, $rootScope, $timeout){

        var init = function(tpl, $scope) {

            var promise;
            $scope = $scope || $rootScope.$new();

            promise = $ionicModal.fromTemplateUrl(tpl, {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.modal = modal;
                return modal;
            });


            $scope.passcode = "";


            // pincode login
            $scope.add = function(value) {
                if($scope.passcode.length < 4) {
                    console.log($scope.passcode)
                    // resetting the error message when starting input again
                    $scope.error = false;
                    $scope.passcode = $scope.passcode + value;

                    if($scope.passcode.length == 4) {
                        $timeout(function () {
                            console.log("The four digit code was entered and is " + $scope.passcode);
                        }, 500);

                        // checking if passcode is correct
                        if($scope.passcode.length == 4 && $scope.passcode == localStorage.getItem('passcode')){
                            $scope.closeModal('pincode');
                        } else {
                            $scope.error = true;
                            //resetting the pinpad to 0
                            $scope.passcode = "";
                        }
                    }
                }
            };



            $scope.delete = function() {
                if($scope.passcode.length > 0) {
                    $scope.passcode = $scope.passcode.substring(0, $scope.passcode.length - 1);
                }
            };

            showPincodeOnNullUser = function () {
                if (!$scope.currentUser)
                    $scope.openModal();
            };


            $scope.openModal = function() {
                $scope.modal.show();
            };
            $scope.closeModal = function() {
                $scope.modal.hide();
            };
            $scope.$on('$destroy', function() {
                $scope.modal.remove();
            });


            return promise;
        }

        return {
            init: init
        }

    });
