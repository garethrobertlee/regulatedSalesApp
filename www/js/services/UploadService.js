angular.module('rsa.services')
    .factory('uploadVideo', function($http,$q,$auth,$rootScope,SaleService){

        window.fileStorage = {
            getFileSystem: function(sale){
                var defer = $q.defer();

                var fileSystem = function(fileSystem){
                    var directoryReader = fileSystem.root.createReader();
                    directoryReader.readEntries(findAFile);
                };
                var findAFile = function(entries) {
                    var thisfile = sale.filename + ".mp4";
                    for (var i = 0; i < entries.length; i++) {
                        if (entries[i].name == thisfile) {
                            defer.resolve(entries[i])
                        }
                    }
                };
                var fail = function(error){
                    console.log(error)
                };
                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, fileSystem, fail);
                return defer.promise
            },
            uploadVideo:function(file,sale){

                var defer = $q.defer();
                var ft = new FileTransfer()
                var path = file.nativeURL
                var name = file.name;
                var server = $auth.apiUrl() + "/api/v1/sales/update_with_video";

                var uploadSuccess = function(result) {
                        defer.resolve(file);
                    file.remove(function(file){
                        console.log("file successfully removed");
                    });
                };

                var uploadError = function(error) {
                    defer.resolve(file);//try this to see if continues after a fail
                    console.log('Error uploading file ' + path + ': ' + error.code);
                };

                ft.upload(path, server, uploadSuccess, uploadError, { fileName: sale.id + ".mp4",headers:JSON.parse(window.localStorage.auth_headers) });

                ft.onprogress = function(progressEvent) {
                    if (progressEvent.lengthComputable) {
                        //loadingStatus.setPercentage(progressEvent.loaded / progressEvent.total);
                        var progress = document.getElementById('progress')
                        var percent = (progressEvent.loaded / progressEvent.total) * 100;
                        var total = progressEvent.total;
                        var loaded = progressEvent.loaded;
                        progress.UpdatePercentage = function(total, loaded) {
                            progress.PercentageComplete = (loaded / total);
                        };
                        document.getElementById("progress_percentage").innerHTML = "Synching Video: " + parseInt(percent) + " percent";
                        document.getElementById("progress_bar").style.width = (progress.offsetWidth - 2) * progress.PercentageComplete + 'px';
                        console.log(document.getElementById("progress_percentage").innerHTML)
                    } else {
                        loadingStatus.increment();
                    }
                };
                return defer.promise
            },
            deleteFile: function(file,sale){
                var defer = $q.defer()
                file.remove(function(file){
                    console.log("file successfully removed");
                    defer.resolve("file successfully removeds")
                });
                return defer.promise
            }
            };

        return {
            sendCurrentFile: function(sale){
                var defer = $q.defer();
                    window.fileStorage.getFileSystem(sale).then(function(data){
                        window.fileStorage.uploadVideo(data,sale).then(function(moredata){
                            defer.resolve(moredata);
                        })
                    });
                return defer.promise
            },
            deleteVideo: function(sale){
                var defer = $q.defer();
                window.fileStorage.getFileSystem(sale).then(function(data){
                    window.fileStorage.deleteFile(data,sale).then(function(moredata){
                        defer.resolve("deleted")
                    })
                });
                return defer.promise
            }
        }
    });
