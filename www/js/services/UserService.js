angular.module('rsa.services')
    .factory('UserService', [
        '$http', function ($http) {

            var user_info = {
                "id": null,
                "stripe":null,
                "email":null,
                "test_user":false,
                "company":null
            };

            return {
                getUser: function(){
                    return user_info
                },
                setUser: function(user){
                    user_info = user
                },
                getId: function () {
                    return user_info.id;
                },
                getCompany: function () {
                    return user_info.company;
                },
                setId: function(id) {
                    user_info.id = id;
                },
                setCompany: function (company) {
                    user_info.company = company;
                },
                setStripeKey: function(stripe) {
                    user_info.stripe = stripe;
                },
                getStripeKey: function() {
                    return user_info.stripe
                },
                setTestUser: function(testUser){
                    user_info.testUser = testUser
                },
                getTestUser: function(){
                    return user_info.testUser
                }
            };
        }
    ]);
