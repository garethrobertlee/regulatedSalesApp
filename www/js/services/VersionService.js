angular.module('rsa.services')
    .factory('VersionService', [
        '$http', function ($http) {

            var local_version = {
                "major": 1,
                "minor": 0,
                "rev":   0
            };

            var latest_version = {
                "major": 1,
                "minor": 2,
                "rev":   0
            };

            return {
                getLatestVersion: function() {
                    return latest_version;
                },
                setLatestVersion: function(version) {
                    latest_version = version;
                },
                getLocalVersion: function() {
                    return local_version;
                },
                getLocalVersionString: function() {
                    return local_version.major + '.' + local_version.minor + '.' + local_version.rev;
                },
                validateVersion: function() {

                    if (latest_version.major > local_version.major)
                    {
                        return 'major';
                    }
                    else if (latest_version.minor > local_version.minor)
                    {
                        return 'minor';
                    }
                    else if (latest_version.rev > local_version.rev)
                    {
                        return 'rev';
                    }
                    else
                    {
                        return 'none';
                    }
                }
            };
        }
    ]);
