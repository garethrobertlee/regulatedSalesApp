angular.module('rsa.services')
    .factory('CredentialsService', [
        '$http', function ($http) {

            var test_credentials = {
                "email": "user",
                "password": "pass",
                "testmode": false,
                "connectTo": "",
                "hasBeenLoggedIn": "",
                "active":false
            };

            return {
                getCredentials: function () {
                    return {
                        //"testmode": test_credentials.testmode,
                        "email": window.localStorage['email'] || '',
                        "password": window.localStorage['password'] || ''
                        //"hasBeenLoggedIn": window.localStorage['hasBeenLoggedIn'] || '',
                        //"admin": test_credentials.admin || '',
                    };
                },
                setCredentials: function(credentials) {
                    //if (credentials.email == 'test@traceauth.com'){
                    //    test_credentials.testmode = true;
                    //    window.localStorage['admin'] = true;
                    //    test_credentials.admin = true;
                    //}
                    //else {
                    //    test_credentials.admin = false;
                    //}

                    window.localStorage['email'] = credentials.email;
                    window.localStorage['password'] = credentials.password;
                    window.localStorage['hasBeenLoggedIn'] = true;
                    window.localStorage['active'] = true;
                    //window.localStorage['use_localhost'] = false;
                    //window.localStorage['admin'] = false;

                },
                //setTestCredentials: function () {
                //    window.localStorage['email'] = "test@traceauth.com";
                //    window.localStorage['password'] = "Trace123456789";
                //    //window.localStorage['use_localhost'] = true;
                //    window.localStorage['admin'] = true;
                //},
                validateCredentials: function() {
                    return true; // TODO - Credentials validation check
                },
                areSet: function() {
                    return window.localStorage['email'] && window.localStorage['password']
                }
                //setLocalhost: function(localhost){
                //    test_credentials.use_localhost = localhost.result
                //},
                //getLocalhost: function(){
                //    return test_credentials.use_localhost;
                //}
            };
        }
    ]);
