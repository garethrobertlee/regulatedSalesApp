angular.module('rsa.services')
    .factory('SaleService', [
        '$http','$auth','$rootScope','$q','UserService','$ionicLoading','$ionicPopup','UserService', function ($http,$auth,$rootScope,$q,UserService,$ionicLoading,$ionicPopup,Userservice) {
            //localStorage.clear();
            var current_sale = {};
            var current_sale_index = -1;
            var finalStatus = ""

            var all_sales = localStorage.getItem("all_sales");//Retrieve the stored data
            all_sales = JSON.parse(all_sales); //Converts string to object
            if(all_sales == null) {
                all_sales = [];
            }

            function createCharge(full_payload){

                return $http.post($auth.apiUrl() + '/api/v1/sales/confirm_and_submit_payment', full_payload).then(function(response) {

                    if (response.data.status == 10)
                    {
                        sale = response.data.sale;
                        successfulSale(sale);
                        //return $location.url('/app/submittedpayment');
                        result = {status: 10, sale: sale}
                        return result
                    }
                    else if (response.data.status == 11)
                    {
                        alert("payment taken but sale no uploaded - please sync sale later");
                        //unsavedSale($scope.currentSale);
                        result = {status: 11}
                        return result
                    }
                    else if (response.data.status == 12 || 14)
                    {
                        alert(response.data.message); //TODO should this be a button instead of redirect immediately after alert?
                        sale = response.data.sale
                        needsPayment(sale);
                        result = {status: 12, sale: sale}
                        return result
                    }
                    else
                    {
                        result = {status: 16, sale: sale}
                        return result
                    }


                }, function(err) {

                    $ionicLoading.hide();
                    var alertPopup = $ionicPopup.alert({
                        title: 'Error Posting Data to the Server\n\n',
                        template: "Sale did not save"
                        // err.status will contain the status code
                    });
                    alertPopup.then(function(res) {
                        console.log(res)

                    });
                    result = {status: 16}
                    return result
                });
            }

            function setFinalStatus(status){
                finalStatus = status
            }
            function getFinalStatus(){
                return finalStatus
            }

            function successfulSale(sale){
                for (var i = 0 ; i < all_sales.length ; i++){
                    if (all_sales[i].name == sale.name){
                        thisSale = all_sales[i];
                        thisSale.id = sale.id;
                        thisSale.name = "Sale " + sale.id;
                        thisSale.dataSyncStatus = true;
                        thisSale.paid = sale.paid;
                        thisSale.syncing = false;
                        thisSale.stripe_publishable_key = sale.stripe_publishable_key;
                        localStorage.setItem("all_sales", JSON.stringify(all_sales));
                    }
                }
            }

            function unsavedSale(sale){
                for (var i = 0; i < all_sales.length ; i++){
                    if (all_sales[i].name == sale.name) {
                        thisSale = all_sales[i];
                        thisSale.paid = true;
                        thisSale.syncing = false;
                        thisSale.stripe_publishable_key = sale.stripe_publishable_key; //TODO this assumes payment OK
                        localStorage.setItem("all_sales", JSON.stringify(all_sales));
                        return true
                    }
                }
            }
            function needsPayment(sale){
                for (var i = 0; i < all_sales.length ; i++) {
                    if (all_sales[i].name == sale.name) {
                        thisSale = all_sales[i];
                        current_sale = sale;
                        thisSale.id = sale.id;
                        thisSale.name = "Sale " + sale.id;
                        thisSale.dataSyncStatus = true;
                        thisSale.paid = false;
                        thisSale.syncing = false;
                        thisSale.token = sale.token;
                        thisSale.stripe_publishable_key = sale.stripe_publishable_key; //TODO this assumes we have a stripe_key
                        localStorage.setItem("all_sales", JSON.stringify(all_sales));
                        return true
                    }
                }
            }

            function createTimeStampUid() {
                var date = new Date();

                var dateStr = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

                var components = [
                    date.getYear(),
                    date.getMonth(),
                    date.getDate(),
                    date.getHours(),
                    date.getMinutes(),
                    date.getSeconds(),
                    date.getMilliseconds()
                ];

                var timestamp = components.join("");

                return {timeStamp: timestamp, dateString: dateStr}
            }

            function editSale(index, sale) {
                all_sales[index] = sale;

                localStorage.setItem("all_sales", JSON.stringify(all_sales));
                return sale;
            }

            return {
                newSale: function(product, customer,user,key) {
                    // Set up a new sale with a given product and customer, assign it a timestamp
                    if(all_sales == null) {
                        all_sales = [];
                    }
                    current_sale = {};
                    current_sale.dateString = createTimeStampUid().dateString;
                    current_sale.timestamp = createTimeStampUid().timeStamp;
                    current_sale.name = 'recording_' + current_sale.timestamp;
                    current_sale.description = {};
                    current_sale.dataSyncStatus = 0;
                    current_sale.videoSyncStatus = false;
                    current_sale.location = null;
                    current_sale.lat = null;
                    current_sale.lng = null;
                    current_sale.product = product;
                    current_sale.customer = customer;
                    current_sale.filename = 'recording_' + current_sale.timestamp;
                    current_sale.signature = null;
                    current_sale.paymentInfo = {};
                    current_sale.payload = {name:null,city:null,price:0,extras:0};
                    current_sale_index = all_sales.push(current_sale) - 1;
                    current_sale.index = current_sale_index
                    current_sale.id = null;
                    current_sale.video = null;
                    current_sale.paid = false;
                    current_sale.syncing = false;
                    current_sale.stripe_publishable_key = key;
                    current_sale.user_id = user.id;
                    current_sale.user_email = user.email;
                    return current_sale;
                },
                successfulSale: function(sale){
                    for (var i = 0; i < all_sales.length ; i++){
                        if (all_sales[i].name == sale.name){
                            thisSale = all_sales[i];
                            thisSale.id = sale.id;
                            thisSale.name = "Sale " + sale.id;
                            thisSale.dataSyncStatus = true;
                            thisSale.paid = true;
                            thisSale.syncing = false;
                            thisSale.stripe_publishable_key = sale.stripe_publishable_key;
                            localStorage.setItem("all_sales", JSON.stringify(all_sales));
                            return true
                        }
                    }
            },
                unsavedSale: function(sale){
                for (var i = 0; i < all_sales.length ; i++){
                    if (all_sales[i].name == sale.name) {
                        thisSale = all_sales[i];
                        thisSale.paid = true;
                        thisSale.syncing = false;
                        thisSale.stripe_publishable_key = sale.stripe_publishable_key; //TODO this assumes payment OK
                        localStorage.setItem("all_sales", JSON.stringify(all_sales));
                        return true
                    }
                }
            },
                deleteSale: function(sale,showmessage){
                    for (var i = 0 ; i<all_sales.length ; i++){
                        if (sale.timestamp == all_sales[i].timestamp){
                            all_sales.splice(i, 1);
                            localStorage.setItem("all_sales", JSON.stringify(all_sales));
                            if (showmessage) {
                                alert("Sale deleted.");
                            }
                            return true;
                        }
                    }
                },
                getSale: function(index) {
                    // Get sale at specifed index
                    return all_sales[index];
                },
                getCurrentSale: function() {
                    return current_sale;
                },
                getCurrentSaleIndex: function() {
                    return current_sale_index;
                },
                setCurrentSale: function(sale) {
                    current_sale = sale;
                    editSale(current_sale_index, current_sale);
                    return current_sale;
                },
                saveSales: function() {
                    localStorage.setItem("all_sales", JSON.stringify(all_sales));
                },
                getAllSales: function() {
                    all_sales = JSON.parse(localStorage.getItem("all_sales"));//Retrieve the stored data
                    return all_sales;
                },
                getSalesNeedingAttention: function(){
                    var defer = $q.defer();
                    salesNeedingAttention = [];
                    all_sales = JSON.parse(localStorage.getItem("all_sales"));//Retrieve the stored data
                    if (all_sales) {
                        console.log("needing attentino")
                        console.log(all_sales)
                        for (var i = 0; i < all_sales.length; i++) {
                            if (all_sales[i].signature && (!all_sales[i].id || all_sales[i].videoSyncStatus != true || all_sales[i].paid == false )) {
                                salesNeedingAttention.push(all_sales[i])
                            }
                            defer.resolve(salesNeedingAttention)
                        }
                    }
                    return defer.promise;
                },
                synchedVideo: function(sale){
                    for (var i = 0;i< all_sales.length ; i++){
                        if (sale.id == all_sales[i].id){
                            //sale.videoSyncSatus = true;
                            all_sales[i].videoSyncStatus = true;
                            all_sales[i].syncing = false;
                        }
                    }
                    localStorage.setItem("all_sales", JSON.stringify(all_sales));
                },
                resetVideo: function(){
                    current_sale = {};
                },
                sendQuote: function(sale) {
                    var defer = $q.defer();
                    console.log(sale)
                    var server = $auth.apiUrl(); // if you set to localhost, wont work from ipad - change to your ip in dev, real url in prod
                    payload = {'amount': 0, 'token': ""};
                    var full_payload = {
                        "payload": JSON.stringify(payload),
                        "sale": JSON.stringify(sale)
                    };



                 //return $http.post(server + '/api/v1/sales.json', sale).success(function(response) {
                    createCharge(full_payload).then(function (result) {
                        if (result.status == 11) {
                            unsavedSale(sale)
                            setFinalStatus(result.status)
                            //return $location.url('/app/submittedpayment');
                        }
                        else if (result.status < 15) {
                            setFinalStatus(result.status)
                            //return $location.url('/app/submittedpayment');
                        }
                        else {
                            defer.resolve(result.status)
                            //return $location.url('/app/dashboard');
                        }
                        defer.resolve(result.status)
                    });
                    return defer.promise
                },
                setFinalStatus: function(status){
                    finalStatus = status
                },
                getFinalStatus: function(){
                    return finalStatus
                },
                makePayment: function(sale) {
                    var defer = $q.defer();
                    pricePence = (parseFloat(sale.payload.price) + parseFloat(sale.payload.extras))*100;
                    var handler = StripeCheckout.configure({
                        key: sale.stripe_publishable_key,
                        email: user.email,
                        amount: pricePence / 100,
                        currency: 'GBP',
                        panelLabel: 'Pay Now',
                        //image: './images/check.png',
                        token: function (token) {
                            console.log("token")
                            payload = {'amount': pricePence, 'token': token.id};

                            full_payload = {
                                "payload": JSON.stringify(payload),
                                "sale": JSON.stringify(sale),
                                "status": 1
                            };
                            // Use the token to create the charge with a server-side script.
                            createCharge(full_payload).then(function (result) {
                                if (result.status == 11) {
                                    unsavedSale(sale)
                                    setFinalStatus(result.status)
                                    //return $location.url('/app/submittedpayment');
                                }
                                else if (result.status < 15) {
                                    setFinalStatus(result.status)
                                    //return $location.url('/app/submittedpayment');
                                }
                                else {
                                    defer.resolve(result.status)
                                    //return $location.url('/app/dashboard');
                                }
                                defer.resolve(result.status)
                            });
                        },
                        closed: function () {
                            //currently unused as unable to distingush between manual close and stripe close events
                        }
                    });


                    // Open Checkout with further options
                    handler.open({
                        name: 'Make the payment',
                        //description: payment.price // TODO - include message about the surcharge
                        amount: pricePence,  // Convert to pence
                        currency: 'GBP',
                        allowRememberMe: false
                    });
                    console.log(document.getElementById('stripeXDM_default757369_provider'))
                    return defer.promise
                }

            };

        }
    ]);
